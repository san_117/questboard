﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class QuestEditor: ScriptableObject
{
    public string title;
    public string description;
    public Category category;
    public int reward;

    public Quest.Paragraph[] parrafos; 

    [ContextMenu("Generate JSON")]
    public void GenerateJson()
    {
        string json = JsonUtility.ToJson(this);
        Debug.Log(json);
    }
}


[System.Serializable]
public class Quest
{
    public string title;
    public string description;
    public Category category;
    public int reward;

    public Paragraph[] parrafos;

    [System.Serializable]
    public class Paragraph
    {
        public string title;
        public string text;
    }
}

public enum Category
{
    Programacion, Arte, Diseño, Testing
}
