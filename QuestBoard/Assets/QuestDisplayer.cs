﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestDisplayer : MonoBehaviour
{
    public Text title;
    public Text description;
    public Text reward;

    public Transform content;

    [Header("Templates")]
    public GameObject paragraph_template;

    public void ShowQuest(string json)
    {
        Quest currentQuest = JsonUtility.FromJson<Quest>(json);

        if (currentQuest != null)
        {
            Display(currentQuest);
            gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("Show invalid qr message here!");
        }
    }

    private void Display(Quest currentQuest)
    {
        title.text = currentQuest.title;
        description.text = currentQuest.description;
        reward.text = "$" + currentQuest.reward;
        title.text = currentQuest.title;

        paragraph_template.SetActive(false);

        for (int i = 0; i < currentQuest.parrafos.Length; i++)
        {
            GameObject pararaph = Instantiate(paragraph_template, content);

            pararaph.GetComponentInChildren<Text>().text = currentQuest.parrafos[i].text;
            pararaph.SetActive(true);
        }
    }
}
